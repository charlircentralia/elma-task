package elma_tests.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.$;

public class MainPage {

    //Elements-locators
    private SelenideElement btn_logo = $(byId("page_start_button"));
    private SelenideElement btn_hover_menu = $((byId("page_start_menu")));
    private SelenideElement menu_content = $(byId("StartMenu_Content"));
    private SelenideElement top_bar_menu_create_task = $(byId("toolbar_btn_tasks_createtasktoobarbutton"));
    private SelenideElement top_bar_save_task = $(byId("toolbar_btn_toolbar-click-save"));
    private SelenideElement task_status_item = $(byClassName("taskStatusLink"));
    private SelenideElement tob_bar_subMenu_task = $(byId("toolbar_btn_tasks_createtasktoobarbutton_task"));
    private SelenideElement top_bar_sendMessage = $(byId("toolbar_btn_messages_sendmessagetoolbarbutton"));
    private SelenideElement message_modal = $(byId("AddChannelMessageWindow"));
    private SelenideElement message_modal_close = $(byValue("Отмена"));

    //left-bar
    private SelenideElement left_bar_menu_item = $(byId("46.i"));
    private SelenideElement clients_search_panel = $(byId("DataFilter_Filter_SearchString"));


    //Getters, setters
    public SelenideElement getBtn_logo() {
        return btn_logo;
    }
    public SelenideElement getBtn_hover_menu() {
        return btn_hover_menu;
    }
    public SelenideElement getMenu_content() { return menu_content; }
    public SelenideElement getTop_bar_menu_create_task() {
        return top_bar_menu_create_task;
    }
    public SelenideElement getTob_bar_subMenu_task() {
        return tob_bar_subMenu_task;
    }
    public SelenideElement getTop_bar_sendMessage() {
        return top_bar_sendMessage;
    }
    public SelenideElement getMessage_modal() {
        return message_modal;
    }
    public SelenideElement getLeft_bar_menu_item() {
        return left_bar_menu_item;
    }
    public SelenideElement getClients_search_panel() {
        return clients_search_panel;
    }
    public SelenideElement getMessage_modal_close() {
        return message_modal_close;
    }
    public void save_task() {
        top_bar_save_task.click();
    }
    public SelenideElement getTask_status_item() {
        return task_status_item;
    }
    //Methods



}
