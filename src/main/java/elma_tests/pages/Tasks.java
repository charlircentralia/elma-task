package elma_tests.pages;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.$;

public class Tasks {

    //Elements-locators
    private SelenideElement field_topic = $(byId("Entity_Subject"));
    private SelenideElement field_executor = $(byId("Executors_Id-input"));
    private SelenideElement executor_item = $(byClassName("userphoto-item"));
    private SelenideElement date_picker_start = $(byId("StartDate_date"));
    private SelenideElement date_picker_end = $(byId("EndDate_date"));
    private SelenideElement select_priority = $(".t-dropdown-wrap span.t-input");
    private SelenideElement field_description = $(byId("Entity_Description"));
    private SelenideElement btn_actions = $(byId("toolbar_btn_toolbar-button-actions"));
    private SelenideElement actions_close = $(byId("toolbar_btn_toolbar-click-close"));
    private SelenideElement close_task_modal_btn = $(byValue("Завершить"));

    //Getters, setters
    public void setField_topic(String topic) {
        field_topic.setValue(topic);
    }
    public void setField_executor(String executor) {
        field_executor.setValue(executor);
    }
    public SelenideElement getExecutor_item() {
        return executor_item;
    }
    public void setDate_picker_start(String startDate) {
        date_picker_start.setValue(startDate);
    }
    public void setDate_picker_end(String endDate) {
        date_picker_end.setValue(endDate);
    }
    public SelenideElement getSelect_priority() { return select_priority; }
    public void setField_description(String description) {
        field_description.setValue(description);
    }
    public SelenideElement getBtn_actions() {
        return btn_actions;
    }
    public SelenideElement getActions_close() {
        return actions_close;
    }
    public SelenideElement getClose_task_modal_btn() {
        return close_task_modal_btn;
    }
    //Methods
}
