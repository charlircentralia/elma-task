package elma_tests.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byId;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;

public class Auth {

    //Elements-locators
    private SelenideElement field_login = $(byId("login"));
    private SelenideElement field_pass = $(byId("password"));
    private SelenideElement check_rememberMe = $(byId("remember_me_container"));
    private SelenideElement btn_logIn = $(byId("LogIn"));
    private SelenideElement btn_profileMenu = $(byId("UserInfo_ExpandButton"));
    private SelenideElement btn_logout = $(byText("Выйти"));


    //Getters, setters
    public void setField_login(String field_login) {
        this.field_login.setValue(field_login);
    }
    public void setField_pass(String field_pass) {
        this.field_pass.setValue(field_pass);
    }
    public SelenideElement getCheck_rememberMe() {
        return check_rememberMe;
    }
    public SelenideElement getBtn_logIn() {
        return btn_logIn;
    }
    public SelenideElement getBtn_profileMenu() {
        return btn_profileMenu;
    }
    public SelenideElement getBtn_logout() {
        return btn_logout;
    }

    //Methods
    public void login(String login, String pass){
        setField_login(login);
        setField_pass(pass);

    }
    public void logOut(){
        getBtn_profileMenu().click();
        getBtn_logout().click();
    }

}
