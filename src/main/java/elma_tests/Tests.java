package elma_tests;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import elma_tests.pages.Auth;
import elma_tests.pages.MainPage;
import elma_tests.pages.Tasks;
import org.testng.Assert;
import org.testng.annotations.*;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;

public class Tests {

    private Data data = new Data();
    private Auth auth = new Auth();
    private Tasks tasks = new Tasks();
    private MainPage mainPage = new MainPage();

    @BeforeSuite
    public void setup() {

        Configuration.baseUrl = data.getWorkspace();
        Configuration.browser = "chrome";
        Configuration.browserSize = "1440x900";
        Configuration.fastSetValue = true;
        Configuration.timeout = 7000;
        Configuration.headless = true;

    }

    @BeforeMethod
    public void start() {
        open(data.getWorkspace());
        auth.login(data.getAdminLogin(), data.getAdminPass());
        auth.getBtn_logIn().click();
    }

    @Test
    public void logoHoverMenuIsAvailableAndClosed() throws InterruptedException{

        //Тест проверяет работоспособность ховер меню логотипа.
        mainPage.getBtn_logo().hover();
        mainPage.getBtn_hover_menu().click();
        Thread.sleep(1000);
        Assert.assertTrue(mainPage.getMenu_content().isDisplayed(), "Главное ховер меню не отображено.");
        $(byId("StartMenu_Padding")).click();
        Assert.assertTrue(mainPage.getMenu_content().is(Condition.not(Condition.visible)), "Главное ховер меню не закрыто.");

    }

    @Test
    public void topBarMenuButtonsIsAvailable() {

        //Тест проверяет работоспособность элменетов верхнего меню.
        mainPage.getTop_bar_menu_create_task().click();
        Assert.assertEquals(WebDriverRunner.url(), data.getWorkspace() + "Tasks/Task/Create", "Несоответствие страницы по url.");
        Assert.assertTrue($(byText("Создать задачу")).isDisplayed(), "Загрузка страницы создания задачи не завершена");

    }

    @Test
    public void messageModalIsAvailable() throws InterruptedException{

        //Тест проверяет работоспособность открытия модального окна для отправки сообщений
        mainPage.getTop_bar_sendMessage().click();
        Thread.sleep(1000);
        Assert.assertTrue(mainPage.getMessage_modal().isDisplayed(), "Модальное окно для отправки сообщений не открыто.");
        mainPage.getMessage_modal_close().click();

    }

    @Test
    public void leftBarMenuButtonsIsAvailable() {

        //Тест проверят работоспособность элементов левого меню.
        mainPage.getLeft_bar_menu_item().click();
        Assert.assertEquals(WebDriverRunner.url(), data.getWorkspace() + "CRM?FilterId=0", "Несоответствие страницы по url.");
        Assert.assertTrue(mainPage.getClients_search_panel().isDisplayed(), "Загрузка страницы с контрагентами не завершена");

    }

    @Test
    public void taskCreationIsWorking() throws InterruptedException{

        //Тест проверяет работоспособность функционала для создания и удаления задач.

        //create task
        mainPage.getTop_bar_menu_create_task().click();
        tasks.setField_topic(data.getTaskTopicName());
        tasks.setField_executor(data.getTaskExecutor());
        tasks.getExecutor_item().click();
        tasks.setDate_picker_start("15092019");
        tasks.setDate_picker_end("20092019");
        tasks.setField_description("Some test text");
        mainPage.save_task();
        Thread.sleep(2000);
        Assert.assertTrue(mainPage.getTask_status_item().exists(), "Задача не отображена на главной странице");
        //delete task
        $(byText(data.getTaskTopicName())).click();
        tasks.getBtn_actions().click();
        tasks.getActions_close().click();
        tasks.getClose_task_modal_btn().click();
        Assert.assertTrue($(byClassName("NewOrder")).is(Condition.not(Condition.exist)), "Задача не закрыта, класс NewOrder отображающий задачи на главной - существует.");

    }

    @AfterMethod
    void tearDown(){

        clearBrowserCookies();
        clearBrowserLocalStorage();
        auth.logOut();

    }

}
