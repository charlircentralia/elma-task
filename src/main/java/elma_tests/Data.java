package elma_tests;

public class Data {

    private String workspace = "http://127.0.0.1:8000/";
    private String adminLogin = "admin";
    private String adminPass = "000000";
    private String taskTopicName = "Test Topic";
    private String taskExecutor = "Администратор ELMA";

    public String getWorkspace() {
        return workspace;
    }
    public void setWorkspace(String workSpace) {
        this.workspace = workSpace;
    }
    public String getAdminLogin() {
        return adminLogin;
    }
    public void setAdminLogin(String adminLogin) {
        this.adminLogin = adminLogin;
    }
    public String getAdminPass() {
        return adminPass;
    }
    public void setAdminPass(String adminPass) {
        this.adminPass = adminPass;
    }
    public String getTaskTopicName() {
        return taskTopicName;
    }
    public String getTaskExecutor() {
        return taskExecutor;
    }
}
