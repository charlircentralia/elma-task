Для запуска тестов необходимо: 

**ВАЖНО**: Для запуска тестов используется baseUrl = http://127.0.0.1:8000 из класса data, для запуска тестов на другом URL необходимо задать его в методе setUp класса Tests.

1. Скачать проект 
2. Открыть проект используя intelliJ IDEA
3. Для запуска тестов нужно использовать конфигурацию Tests и кнопку run -> https://prnt.sc/p6ce2h
4. Для изменения конфигурация теста необходимо менять значение Configuration в методе setup

2. Отчётность прогона тестов будет отображена в окне run -> https://prnt.sc/p6cei6

3. На настройку проекта и написание тестов было потрачено 7 часов.


Тесты:
1. Проверка отображения hover меню логотипа
2. Проверка работоспособности ссылок для верхнего меню.
3. Проверка отображения модального окна для отправки сообщений
4. Проверка работоспособности ссылок для левого меню.
5. Создание задачи, заполнение полей, проверка отображения задачи на главной странице, удаление задачи, проверка удаления.